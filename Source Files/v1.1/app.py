import eel

from modules.loginFunctions import *
from modules.editorFunctions import *
from modules.tableFunctions import *

eel.init("web", allowed_extensions=[".js", ".html"])

eel.start("editor.html", mode="default", port=8080)
