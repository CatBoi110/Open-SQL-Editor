const dropDown = document.querySelector("#Backend-Dropdown");
const inputContainer = document.querySelector(".Container");
const submitButton = document.querySelector("#Submit");
const deleteButton = document.querySelector("#Delete");

var info = [];

submitButton.addEventListener("click", submitLogin);
deleteButton.addEventListener("click", deleteLogin);

function populateInputFields(){
    clearInputFields()

    if (dropDown.value ==  "MySQL" || dropDown.value == "MS Azure"){
        let server = document.createElement("input");
        let database = document.createElement("input"); 
        let username = document.createElement("input");
        let password = document.createElement("input");

        server.placeholder = "Server";
        database.placeholder = "Database";
        username.placeholder = "Username";
        password.placeholder = "Password";
        password.type = "password";

        server.classList.add("Input-Box");
        database.classList.add("Input-Box");
        username.classList.add("Input-Box");
        password.classList.add("Input-Box");

        inputContainer.appendChild(server);
        inputContainer.appendChild(database);
        inputContainer.appendChild(username);
        inputContainer.appendChild(password);

        let submit = submitButton.cloneNode(true);

        submit.style.visibility = "visible";

        submit.addEventListener("click", submitLogin);

        inputContainer.appendChild(submit);
    } else if (dropDown.value == "SQLite"){
        let database = document.createElement("input");
        database.placeholder = "Database";
        database.classList.add("Input-Box");
        inputContainer.appendChild(database);

        let disclaimer = document.createElement("h3");
        disclaimer.innerHTML = "(If the desired SQLite database does not exist, a new one will be created)";
        inputContainer.appendChild(disclaimer);

        let submit = submitButton.cloneNode(true);
        submit.style.visibility= "visible";
        submit.addEventListener("click", submitLogin);
        inputContainer.appendChild(submit);
    }
        
}

function clearInputFields(){
    let children = inputContainer.children
    
    for (let i = children.length - 1; i > 0; i --){
        children[i].remove();
    }
}

async function submitLogin(){
    let info = []

    let backend = dropDown.value;
    info.push(backend);

    for (let i = 0; i < inputContainer.children.length - 1; i ++){
        let element = inputContainer.children[i];
        if (element.nodeName == "INPUT"){    
            if (element.value == ""){
                alert("Please enter a value for: " + element.placeholder);
            } else {
                info.push(element.value);
            }
        }
    }

    if (backend == "SQLite"){
        if (info.length == 2){
            eel.saveLogin(info);
            alert("Selected Database");
        }
    } else if (backend == "MySQL" || backend == "MS Azure"){
        if (info.length == 5){
            eel.saveLogin(info);
            alert("Login Saved");
        }
    }
}

function deleteLogin(){
    if (confirm("Are you sure you want to delete the saved login?")){
        eel.deleteLogin();
    }
}
