const table = document.querySelector(".Table");
const refreshButton = document.querySelector("#Refresh");
const editButton = document.querySelector("#Edit");
const delButton = document.querySelector("#Delete");
const applyButton = document.querySelector("#Apply");
const revertButton = document.querySelector("#Revert");
const newButton = document.querySelector("#New");
const cancelButton = document.querySelector("#Cancel");
const dropDown = document.querySelector("#Table-Dropdown");

var columnFields = [];

var inputTemplate = "<input class = 'Input-Field' size = '5' placeholder = '";
var dateInputTemplate = '<input type = "date" class = "Input-Field">';

newButton.addEventListener("click", addRowPrompt);

loginIn();

async function loginIn(){
    if (await eel.isLoginEmpty()() == false){

        let backend = await eel.getBackend()();

        if (backend == "MySQl" || backend == "MS Azure"){
            let failedLoginAttempts = 0;

            while (true){

                psk = askForPassword();

                if (psk == null){
                    break;
                }

                if (await eel.connectToBackend(psk)()){
                    initTable();   

                    alert("Login Successful!");

                    break;
                } else {
                    if (failedLoginAttempts < 3){
                        alert("Please Try Again.");
                        failedLoginAttempts ++;
                    } else {
                        alert("Please Verify Server Login. Use the Login tab to reset saved login information.");
                        break;
                    }
                }
            }

        } else if (backend == "SQLite"){
            
            if (await eel.connectToBackend("N/A")()){
                initTable();

                alert("Connected to database!")

            } else {
                alert("Something went wrong. Please verify database information.");
            }
        }
    } else {
        alertMissingLogin();
    }
}

async function initTable(){
    clearTable();

    await new Promise(resolve => setTimeout(resolve, 1000)); 

    let listOfTables = await eel.getAvailableTables()();

    if (dropDown.length == 1){
        for (let k = 0; k < listOfTables.length; k ++){
            let option = document.createElement("option");
            dropDown.appendChild(option);
        }
    }

    for (let j = 1; j <= listOfTables.length; j ++){
        dropDown.options[j].innerHTML = listOfTables[j - 1];
    }

    if (dropDown.value != "Select A Table"){
        eel.setTable(dropDown.value);
        columnFields = await eel.initTable()();

        let columnNum = 0;
        let row = table.insertRow(0);

        for (let i = 0; i < columnFields.length; i ++){
            row.insertCell(columnNum);
            row.cells[columnNum].innerHTML = columnFields[i][0];
            row.cells[columnNum].style = "padding: 0px 10px 0px 10px;";
            columnNum ++;
        }

        row.insertCell(columnNum);

        let refresh = refreshButton.cloneNode(true);

        refresh.style.visibility = "visible";
        row.cells[columnNum].appendChild(refresh);

        refresh.addEventListener("click", function(){refreshTable()});

        refreshTable();

    } else {
        clearTable();
    }
}

async function refreshTable(){
    clearTable();

    await new Promise(resolve => setTimeout(resolve, 1000)); 

    let tableData = []
    tableData = await eel.refresh()();

    let columnNum = 0;

    for (let rowNum = 0; rowNum < tableData.length; rowNum ++){

        let row = table.insertRow(rowNum + 1);

        for (columnNum = 0; columnNum < tableData[rowNum].length; columnNum ++){
            row.insertCell(columnNum)
            row.cells[columnNum].innerHTML = tableData[rowNum][columnNum];
        }

        row.insertCell(columnNum);

        let edit = editButton.cloneNode(true);
        let del = delButton.cloneNode(true);

        edit.style.visibility = "visible";
        del.style.visibility = "visible";

        row.cells[columnNum].appendChild(edit);
        row.cells[columnNum].appendChild(del);

        edit.addEventListener("click", function(){editRowPrompt(row)});
        del.addEventListener("click", function(){deleteRowPrompt(del)});

    }

    newButton.style.visibility = "visible";

}

function clearTable(){
    for (let i = 0; i < table.rows.length; i ++){
        if (i > 0){
            table.deleteRow(i);
            i --;
        }

        if (table.rows.length == 2){
            i++;
        }
    }

}


function addRowPrompt(){
    newButton.style.visibility = "hidden";
    let rowIndex = table.rows.length - 1;
    let row = table.insertRow(rowIndex);

    let columnNumber = 0;

    for (let i = 0 ; i < columnFields.length; i ++){
        row.insertCell(columnNumber);

        let columnName = columnFields[i][0];
        let dataType = columnFields[i][1];
        let isNullable = columnFields[i][2];
        let autoIncrement = columnFields[i][3];

        if (dataType.match("date")){
            row.cells[columnNumber].innerHTML = dateInputTemplate;
        } else if (dataType.match("datetime")){
            row.cells[columnNumber].innerHTML = dateTimeInputTemplate
        } else {
            row.cells[columnNumber].innerHTML = inputTemplate + columnName + "'>";
        }

        if (isNullable === "NO"){
            if (autoIncrement === true){
                row.cells[columnNumber].innerHTML = "N/A";
            } else {
                row.cells[columnNumber].firstChild.classList.add("Required-Field");
            }
        }

        columnNumber ++;
    }

    let cancel = cancelButton.cloneNode(true);
    let apply = applyButton.cloneNode(true);

    apply.style.visibility = "visible";
    cancel.style.visibility = "visible";

    row.appendChild(apply);
    row.appendChild(cancel);

    cancel.addEventListener("click", function(){
        table.deleteRow(cancel.parentElement.rowIndex);
        newButton.style.visibility = "visible";
    });

    apply.addEventListener("click", function() {
        let newData = [];

        let isVaildData = true;

        let numberRequiredFields = 0;

        for (let i = 0; i < row.cells.length; i++){
            if (columnFields[i][2] == "NO" && columnFields[i][3] == false){
                numberRequiredFields ++;
            }

            let value = row.cells[i].firstChild.value;

            if (value !== undefined){
                if (checkField(value, columnFields[i])){
                    newData.push([columnFields[i][0], value]);
                } else {
                    alert(i);
                    isVaildData = false;
                }
            }

        }

        if (isVaildData === true){
            let numberNotNullFields = 0;

            for (let j = 0; j < newData.length; j ++){
                if (newData[j][1] !== "`"){
                    numberNotNullFields ++;
                }
            }

            if (numberNotNullFields >= numberRequiredFields){
                eel.addRow(newData);

                refreshTable();
            } else {
                alert("Please fill in at least " + numberRequiredFields + " field(s)");
            }
        }
    });
}

function deleteRowPrompt(btn){

    let row = btn.parentElement.parentElement;

    if (confirm("Are you sure you want to delete this row?")){
        valuesArray = []


        for (let i = 0; i < row.children.length; i++){
            valuesArray.push(row.cells[i].innerHTML);
        }
        
        valuesArray.pop(valuesArray.length - 1);

        eel.deleteRow(valuesArray);
    }

    refreshTable();
}

function editRowPrompt(row){
    let hasKeyField = false;
    let keyField = null;
    let numberRequiredFields = 0;

    for (let i = 0; i < columnFields.length; i ++){
        if (columnFields[i][2] == "NO" || columnFields[i][3] == true){
            hasKeyField = true;
            keyField = [columnFields[i][0], row.cells[i].innerHTML];

            numberRequiredFields ++;
        }
    }

    if (hasKeyField){
        let oldData = [];

        let columnNumber = 0;

        for (let i = 0; i < columnFields.length; i ++){
            oldData.push(row.cells[columnNumber].innerHTML);
            columnNumber ++;
        }
        
        row.cells[columnNumber].innerHTML = "";

        let apply = applyButton.cloneNode(true);
        let revert = revertButton.cloneNode(true);

        apply.style.visibility = "visible";
        revert.style.visibility = "visible";
        
        columnNumber = 0;

        for (let i = 0; i < columnFields.length; i ++){
            let columnName = columnFields[i][0];
            let dataType = columnFields[i][1];
            let isNullable = columnFields[i][2];
            let autoIncrement = columnFields[i][3];

            if (dataType.match("date")){
                row.cells[columnNumber].innerHTML = dateInputTemplate;
                row.cells[columnNumber].firstChild.value = oldData[columnNumber];
            } else if (dataType.match("datetime")){
                row.cells[columnNumber].innerHTML = dateTimeInputTemplate
                row.cells[columnNumber].firstChild.value = oldData[columnNumber];
            } else {
                row.cells[columnNumber].innerHTML = inputTemplate + columnName + "'>";
                row.cells[columnNumber].firstChild.value = oldData[columnNumber];
            }

            if (isNullable === "NO"){
                if (autoIncrement === true){
                    row.cells[columnNumber].innerHTML = oldData[columnNumber];
                } else {
                    row.cells[columnNumber].firstChild.classList.add("Required-Field");
                }
            }

            columnNumber ++;
        }

        row.cells[columnNumber].appendChild(apply);
        row.cells[columnNumber].appendChild(revert);

        apply.addEventListener("click", function(){
            let newData = [];
            let isVaildData = true;

            for (let i = 0; i < row.children.length - 1; i ++){
                let element = row.cells[i];
                let value = null;


                if (element.firstChild.value !== undefined){
                    value = element.firstChild.value;

                    newData.push([table.rows[0].cells[i].innerHTML, value]);

                    if (checkField(value, columnFields[i]) === false){
                        isVaildData = false;
                    }
                }

            }    

            if (isVaildData === true){
                let numberNotNullFields = 0;

                for (let j = 0; j < newData.length; j ++){
                    if (newData[j][1] !== ""){
                        numberNotNullFields ++;
                    }
                }

                if (numberNotNullFields >= numberRequiredFields){
                    eel.editRow(newData, keyField);

                    refreshTable();
                } else {
                    alert("Please fill in at least " + numberRequiredFields + " field(s)");
                }
            }
        });

        revert.addEventListener("click", function(){
            for (let i = 0; i < oldData.length; i++){
                row.cells[i].innerHTML = oldData[i];
            }

            row.cells[oldData.length].innerHTML = "";

            let edit = editButton.cloneNode(true);
            let del = delButton.cloneNode(true);

            edit.style.visibility = "visible";
            del.style.visibility = "visible";

            row.cells[oldData.length].appendChild(edit);
            row.cells[oldData.length].appendChild(del);

            edit.addEventListener("click", function(){editRowPrompt(row)});
            del.addEventListener("click", function(){deleteRowPrompt(del)});
        });

    } else {
        alert("This table does not have a key field. The update row function will not work unless the table has a key field. Please try again.");
    }
}


function checkField(value, field){
    if (field[2] == "NO"){
        if (field[1] == "int identity"){
            return true
        } else if (value === ""){
            alert("Please Enter A Non-empty Response For: " + field[0]);
        }

    } else if (field[2] == "YES"){
        if (value === ""){
            return true;
        }
    }

    if (field[1].match("char|varchar|binary|varbinary|tinyblob|tinytext|text|blob|meduimtext|mediumblob|longtext|longblob|nchar|nvarchar|ntext") && value.match("0|1|2|3|4|5|6|7|8|9")){
        alert("Please Enter A Text Value For: " + field[0]);

    } else if (field[1].match("decimal|numeric|smallmoney|money|float|double|dec") && value.indexOf(".") == -1 && value !== ""){
        alert("Please Enter A Decimal Number For: " + field[0]);

    } else if (field[1].match("bit|tinyint|bool|boolean|meduimint|int|integer|bigint") && (!value.match("0|1|2|3|4|5|6|7|8|9") || value.indexOf(".") != -1) && value !== ""){
        alert("Please Enter A Whole Number Value For: " + field[0]);

    } else {
        return true;
    }

    return false;
}

eel.expose(askForPassword);
function askForPassword(){
    while (true){
        let psk = prompt("Enter password to Database");

        if (psk != null){
            if (psk !== ""){
                return psk;
            } else {
                alert("Password Must Not Be Empty.");
            }
        } else {
            alert("Reload this page to return to password prompt");
            break;
        }
    }
}

function alertMissingLogin(){
    alert("No Login Information was found. Please enter login info in the Login Tab");
}

eel.expose(alertSuccessfulQuery);
function alertSuccessfulQuery(){
    alert("Success!");
}

eel.expose(alertErrorCode);
function alertErrorCode(msg){
    alert("Error: " + msg);
}