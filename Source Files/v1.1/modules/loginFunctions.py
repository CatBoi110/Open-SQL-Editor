import eel
import pyodbc
import os

from dotenv import dotenv_values
from passlib.hash import bcrypt

from modules.editorFunctions import setCursor

loginFile = ".env"

@eel.expose
def saveLogin(array):
    global backend, server, database, username, password

    if array[0] == "MySQL" or array[0] == "MS Azure":
        array[4] = bcrypt.hash(array[4])
        
        with open(loginFile, "w") as file:
            file.write("backend=" + array[0] + "\n")
            file.write("server=" + array[1] + "\n")
            file.write("database=" + array[2] + "\n")
            file.write("username=" + array[3] + "\n")
            file.write("password=" + array[4] + "\n")

        file.close()

    elif array[0] == "SQLite":
        with open(loginFile, "w") as file:
            file.write("backend=" + array[0] + "\n")
            file.write("server=N/A" + "\n")
            file.write("database=" + array[1] + "\n")
            file.write("username=N/A" + "\n")
            file.write("password=N/A" + "\n")
        
        file.close()

    values = dotenv_values(loginFile)

    backend = values["backend"]
    server = values["server"]
    database = values["database"]
    username = values["username"]
    password = values["password"]


@eel.expose
def deleteLogin():
    with open(loginFile, "w") as file:
        file.truncate(0)

def isValidPassword(psk):
    return bcrypt.verify(psk, password)

@eel.expose
def connectToBackend(psk):
    errorCode = ""
    connection = None
    
    if backend == "MySQL" or backend == "MS Azure":
        if isValidPassword(psk):
            try:
                if backend == "MySQL":
                    connection = pyodbc.connect("DRIVER=MySQL ODBC 8.0 ANSI Driver;SERVER="+server+";DATABASE="+database+";UID="+username+";PWD="+psk+";charset=utf8mb4;") 
                elif backend == "MS Azure":
                    connection = pyodbc.connect('DRIVER={ODBC Driver 18 for SQL Server};SERVER='+server+';DATABASE='+database+';ENCRYPT=yes;UID='+username+';PWD='+psk)
            except pyodbc.Error as ex:
                errorCode = ex.args[1]
        else:
            errorCode = "Incorrect Password"
    elif backend == "SQLite":
        try:
            try:
                connection = pyodbc.connect("DRIVER=SQLite;Database="+database)
            except:
                connection = pyodbc.connect("DRIVER=SQLite3 ODBC Driver;Database="+database)
        except pyodbc.Error as ex:
            errorCode = ex.args[1]
    if errorCode != "":
        eel.alertErrorCode(errorCode)
        return False
    else:
        cursor = connection.cursor()
        setCursor(cursor)
        return True
    

@eel.expose
def isLoginEmpty():
    try:
        if os.stat(loginFile).st_size == 0:
            return True
        else:
            return False
    except:
        return True

@eel.expose
def getBackend():
    return backend

@eel.expose
def getCursor():
    try:
        values = dotenv_values(loginFile)

        cursor = values["cursor"]

        return cursor

    except pyodbc.Error:
        return ""

if not os.path.isfile(loginFile):
    open(loginFile, "w").close()

if not isLoginEmpty():
    values = dotenv_values(loginFile)

    backend = values["backend"]
    server = values["server"]
    database = values["database"]
    username = values["username"]
    password = values["password"]
