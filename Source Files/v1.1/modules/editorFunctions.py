#6/12/23 11:48
import pyodbc 
import eel
from dotenv import dotenv_values

table = ""
cursor = ""

try:
    values = dotenv_values(".env")
    backend = values["backend"]
except:
    backend = None


@eel.expose
def initTable():
    '''
    MySQL: Extra from schema.columns
    MS Azure: exec sp_columns
    SQLite: sqlite_master
    '''
    global columnFields

    columnFields = []

    # ColumnField Format: 
    # 1. Name (String)
    # 2. Data_Type (String)
    # 3. Is_nullable (Yes / No)
    # 4. Auto_incrmenets (True / False)
    
    if backend == "MySQL":
        sendQuery("select column_name, data_type, is_nullable, EXTRA from information_schema.columns where table_name = '" + table + "';", False)
        formatArray(list(str(cursor.fetchall())), columnFields)


    elif backend == "MS Azure":
        sendQuery("exec sp_columns " + table + ";", False)
        temp = cursor.fetchall()

        for i in range(len(temp)):
            if (temp[i][5] == "int identity"):
                columnFields.append([temp[i][3], temp[i][5], temp[i][17], True])
            else:
                columnFields.append([temp[i][3], temp[i][5], temp[i][17], False])


    elif backend == "SQLite":
        sendQuery("Pragma table_info(" + table + ");", False)
        temp = cursor.fetchall()

        for i in range(len(temp)):
            if temp[i][5] == 0:
                columnFields.append([temp[i][1], temp[i][2], "YES", False])
            elif temp[i][5] == 1:
                # In SQLite a combination of key field and integer will auto increment if no value is provided. If one is then it will overide the increment
                if temp[i][2] == "INTEGER":
                    columnFields.append([temp[i][1], temp[i][2], "YES", True])
                else:
                    columnFields.append([temp[i][1], temp[i][2], "NO", False])

    return columnFields

@eel.expose
def getAvailableTables():        
    listOfTables = []

    if backend == "MySQL":
        sendQuery("show tables;", False)
    elif backend == "MS Azure":
        sendQuery("select table_name from information_schema.tables;", False)
    elif backend == "SQLite":
        sendQuery("select name from sqlite_master where type = 'table';", False)

    formatArray(list(str(cursor.fetchall())), listOfTables)

    return listOfTables


@eel.expose
def refresh():
    tableData = [] 

    sendQuery("select * from " + table + ";", False);
    
    row = cursor.fetchall()
    tempArray = []

    for i in range(len(row)):
        for j in range(len(row[i])):

            if row[i][j] is None:
                row[i][j] = ""

            tempArray.append(str(row[i][j]).strip())

        tableData.append(tempArray)
        tempArray = []

    return tableData


@eel.expose
def addRow(data):
    query = "insert into " + table + " ("

    for j in range(len(data)):
        if j < len(data) - 1:
            query += str(data[j][0]) + ", "
        elif j == len(data) - 1:
            query += str(data[j][0]) + ")"

    query += " values("

    for i in range(len(data)):
        if i < len(data) - 1:
            query += "'" + str(data[i][1]) + "', "
        elif i == len(data) - 1:
            query += "'" + str(data[i][1]) + "');"
    
    sendQuery(query, True)


@eel.expose
def deleteRow(row):
    query = "delete from " + table + " where "

    columnNumber = 0


    for i in range(len(columnFields)):
        query += str(columnFields[i][0]) 

        if str(row[columnNumber]) == "None":
            query += " is null"
        else:
            query += " = '" + str(row[columnNumber]) + "'"
        
        if i < len(columnFields) - 1:
            query += " and "
        else:
            query += " ;"

        columnNumber += 1

    sendQuery(query, True)


@eel.expose
def editRow(newData, keyField):
    query = "update " + table + " set " 
    
    for i in range(len(newData)):
        query += str(newData[i][0]) + " = "

        if str(newData[i][1]) == '':
            query += " null"
        else:
            query += "'" + str(newData[i][1]) + "'"

        if i < len(newData) - 1:
            query += ", "

    query += " where " + keyField[0] + " = " + keyField[1] + ";"

    sendQuery(query, True)

@eel.expose
def setTable(newTable):
    global table

    table = newTable

def setCursor(csr):
    global cursor
    
    cursor = csr

def sendQuery(query, alertOnSuccess):
    try:
        cursor.execute(query)
    except pyodbc.Error as ex:
        eel.alertErrorCode(ex.args[1])
    else:
        if alertOnSuccess == True:
            eel.alertSuccessfulQuery()
            cursor.commit()


def formatArray(startArr, endArr):
    tempList = []
    tempCharList = []

    tempString = ""

    for i in range(0, len(startArr)):
        if (startArr[i] not in ["[", "(", "'", ",", ")", "]"]):
            tempList.append(startArr[i])

        if (startArr[i] == ")"):
            tempCharList.append(tempList)
            tempList = []

    for i in range(len(tempCharList)):
        for j in range(len(tempCharList[i])):
            tempString += tempCharList[i][j];

        endArr.append(tempString.split(" ")) 
        tempString = ""

    for i in range(1, len(endArr)):
        endArr[i].pop(0)

    return endArr
